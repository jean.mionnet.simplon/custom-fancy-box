// HTML ELEMENTS
const imgs = document.querySelectorAll('.fancy_items');
const fancy_box = document.getElementById('custom_fancybox');
const img_zoom = document.getElementById('img_zoom');

// FOR EACH IMG OF THE GALLERY
for (const img of imgs) {
    // IMG ON CLICK
    img.addEventListener('click', (e) => {
        img_zoom.setAttribute('src', e.target.src); // ADD SRC AT FANCY BOX
        fancy_box.style.visibility = 'visible'; // MAKE IT VISIBLE
        fancy_box.style.animation = 'filter 1s'; // ANIMATE BOX
        img_zoom.style.animation = 'slideInDown 1s'; // ANIMATE IMG
    })
    // FANCY BOX ON CLICK WHEN SHE'S OPENED
    fancy_box.addEventListener('click', () => {
        fancy_box.style.visibility = 'hidden';
        fancy_box.style.animation = '';
        img_zoom.style.animation = '';
    })
    // DOCUMENT ON CLICK WHEN FANCY BOX IS OPENED
    document.addEventListener('keydown', (cmd) => {
        if (cmd.keyCode === 27) {
            fancy_box.style.visibility = 'hidden';
            fancy_box.style.animation = '';
            img_zoom.style.animation = '';
        }
    })
}